import Vue from 'vue'
import Router from 'vue-router'
import index from '../pages/index/index'
import adminOverview from '../pages/admin/overview'
import addSchool from '../pages/settings/addSchool'
import addFloor from '../pages/settings/addFloor'
import schoolList from '../pages/settings/schoolList'
import settingSchool from '../pages/settings/settingSchool'
import floorList from '../pages/settings/floorList'
import login from '../pages/admin/login'
import wxuser from '../pages/shuju/wxuser'
import senderList from '../pages/admin/senderList'
import typeList from '../pages/shops/typeList'
import addShop from '../pages/shops/addShop'
import shopList from '../pages/shops/shopList'
import goodsList from '../pages/shops/goodsList'
import foodsList from '../pages/shops/foodsList'
import addFood from '../pages/shops/addFood'
import orderzs from '../pages/order/orderzs'
import orderzq from '../pages/order/orderzq'
import orderpt from '../pages/order/orderpt'
import orderts from '../pages/order/orderts'
import rechage from '../pages/rechage/rechage'
import sliderList from '../pages/settings/sliderList'
import supplierAllBack from '../pages/admin/supplierAllBack'
import settingAllBack from '../pages/admin/settingAllBack'
import jiFenList from '../pages/jifen/jiFenList'
import addJiFen from '../pages/jifen/addJiFen'
import jiFenOrder from '../pages/jifen/jiFenOrder'
import ershouList from '../pages/ershou/ershouList'
import noticeList from '../pages/notice/noticeList'
import pinglun from '../pages/pinglun/pinglun'
import tixian from '../pages/shuju/tixian'
import shopday from '../pages/shuju/shopday'
import runday from '../pages/shuju/runday'
import shopdayxq from '../pages/shuju/shopdayxq'
import couponList from '../pages/settings/couponList'
import richList from '../pages/settings/richList'
import richSetting from '../pages/settings/richSetting'
Vue.use(Router)

export default new Router({
  routes: [
    {path:'/login',name:'login',component:login },
    {path: '/', name: 'index', component: index, children: [
      {path:'/adminOverview',name:'adminOverview' ,component: adminOverview},
      {path:'/senderList',name:'senderList' ,component: senderList},
      {path:'/addSchool',name:'addSchool' ,component: addSchool},
      {path:'/addFloor',name:'addFloor' ,component: addFloor},
      {path:'/schoolList',name:'schoolList' ,component: schoolList},
      {path:'/settingSchool',name:'settingSchool' ,component: settingSchool},
      {path:'/floorList',name:'floorList' ,component: floorList},
      {path:'/wxuser',name:'wxuser' ,component: wxuser },
      {path:'/typeList',name:'typeList' ,component: typeList},
      {path:'/addShop',name:'addShop' ,component: addShop },
      {path:'/shopList',name:'shopList' ,component: shopList },
      {path:'/goodsList',name:'goodsList' ,component: goodsList },
      {path:'/foodsList',name:'foodsList' ,component: foodsList },
      {path:'/addFood',name:'addFood' ,component: addFood },
      {path:'/orderzs',name:'orderzs' ,component: orderzs },
      {path:'/orderzq',name:'orderzq' ,component: orderzq },
      {path:'/orderpt',name:'orderpt' ,component: orderpt },
      {path:'/orderts',name:'orderts' ,component: orderts },
      {path:'/rechage',name:'rechage' ,component: rechage },
      {path:'/sliderList',name:'sliderList' ,component: sliderList },
      {path:'/jiFenList',name:'jiFenList' ,component: jiFenList },
      {path:'/addJiFen',name:'addJiFen' ,component: addJiFen },
      {path:'/jiFenOrder',name:'jiFenOrder' ,component: jiFenOrder },
      {path:'/ershouList',name:'ershouList' ,component: ershouList },
      {path:'/noticeList',name:'noticeList' ,component: noticeList },
      {path:'/pinglun',name:'pinglun' ,component: pinglun },
      {path:'/supplierAllBack',name:'supplierAllBack' ,component: supplierAllBack },
      {path:'/settingAllBack',name:'settingAllBack' ,component: settingAllBack },
      {path:'/tixian',name:'tixian' ,component: tixian },
      {path:'/shopday',name:'shopday' ,component: shopday },
      {path:'/runday',name:'runday' ,component: runday },
      {path:'/shopdayxq',name:'shopdayxq' ,component: shopdayxq },
      {path:'/couponList',name:'couponList' ,component: couponList },
      {path:'/richList',name:'richList' ,component: richList },
      {path:'/richSetting',name:'richSetting' ,component: richSetting }
    ]}
  ]
})
