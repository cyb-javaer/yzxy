var app = getApp();
var that;

Page({

  data: {
    showdetail: false,
    showdetail2: false,
    showdetail3: true,
    showPerson: false,
    showPhone: false,
    showNumber: false,
    showSchool: false,
    schoolList: [],
    schoolNames: [],
    schoolName: '请选择学校',
    schoolId:0,
    sender: '',
    contactPerson: '',
    contactPhone: '',
    contactNumber: ''
  },

  onLoad: function (options) {
    that = this;
    that.findSchool()
  },

  onShow: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.login({
      success: function (res) {
        wx.request({
          url: app.globalData.IP + '/ops/user/wx/login', //仅为示例，并非真实的接口地址
          data: {
            code: res.code,
            schoolId: 23
          },
          dataType: 'json',
          method: 'GET',
          header: {
            'content-type': 'application/x-www-form-urlencoded',
          },
          success(res) {
            //成功获取用户信息
            wx.hideLoading()
            wx.setStorageSync('token', res.data.params.token);
            wx.setStorageSync('user', res.data.params.user);
            if (res.data.params.user.phone.length == 11) {
              that.findRunner(res.data.params.user.phone)
            } else {
              wx.redirectTo({
                url: '/pages/bindUser/bindUser',
              })
            }
          },
          fail: function () {
            wx.hideLoading()
            wx.showToast({
              title: '网络状态实在太差，请退出小程序重新试试',
              icon: 'none',
              duration: 2000,
              mask: true,
            })
          }
        })
      }
    })
  },

  findSchool: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: app.globalData.IP + '/ops/school/find', //仅为示例，并非真实的接口地址
      data: {
        page: 1,
        size: 100,
        orderBy: 'sort desc',
        queryType: 'wxuser'
      },
      dataType: 'json',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      success(res) {
        if (res.data.code) {
          //成功
          wx.hideLoading()
          for (let i = 0; i < res.data.params.list.length; i++) {
            if (res.data.params.list[i].name == '椰子校园配送版（用户勿选）') {
              res.data.params.list.splice(i, 0)
            } else if (res.data.params.list[i].name == '椰子校园商户版（用户勿选）') {
              res.data.params.list.splice(i, 0)
            } else if (res.data.params.list[i].name == '椰子校园合伙人版（用户勿选）') {
              res.data.params.list.splice(i, 0)
            } else {
              that.data.schoolNames.push(res.data.params.list[i].name);
              that.data.schoolList.push(res.data.params.list[i]);
            }
          }
          that.setData({
            schoolNames: that.data.schoolNames,
            schoolList: that.data.schoolList
          })
          wx.setStorage({
            key: 'hoslist',
            data: res.data.params.list,
          })
        } else {
          wx.hideLoading()
          wx.showToast({
            title: res.data.msg,
            image: '/images/tanHao.png',
            duration: 2000,
            mask: true
          })
        }
      }
    })
  },

  // 选择学校函数
  bindPickerChange: function (e) {
    that.setData({
      schoolId: that.data.schoolList[e.detail.value].id,
      schoolName: that.data.schoolList[e.detail.value].name,
    })
  },

  //查询配送员结果
  findRunner: function (phone) {
    if(phone != ''){
      wx.showLoading({
        title: '加载中',
        mask: true
      })
      app.post('/ops/sender/nocheck/findbyphone', {
        phone: phone
      }, function (res) {
        if (res.data.code) {
          if (!res.data.params.sender) {
            wx.showModal({
              title: '提示',
              content: '根据绑定手机号未查到配送员信息',
              showCancel: false,
              confirmColor: '#3797ee',
              confirmText: '朕知道了',
              success: function (res) { }
            })
            that.setData({
              showdetail: true,
              showdetail2: false,
              showdetail3: false,
              showPhone: true,
              contactPhone: phone,
            })
          } else if (res.data.params.sender.exam == "待审核") {
            wx.showModal({
              title: '提示',
              content: '您的注册信息正在审核中，请耐心等待',
              showCancel: false,
              confirmColor: '#3797ee',
              confirmText: '朕知道了',
              success: function (res) { }
            })
            that.setData({
              showdetail: false,
              showdetail2: true,
              showdetail3: false,
            })
          } else if (res.data.params.sender.exam == "审核失败") {
            wx.showModal({
              title: '提示',
              content: '您的注册信息审核失败，请在椰子校园小程序中重新注册',
              showCancel: false,
              confirmColor: '#3797ee',
              confirmText: '朕知道了',
              success: function (res) { }
            })
            that.setData({
              sender: res.data.params.sender,
              schoolId: res.data.params.sender.schoolId,
              showSchool: true,
              showPerson: true,
              showPhone: true,
              showNumber: true,
              schoolName:'已无法重选',
              contactPerson: res.data.params.sender.name,
              contactPhone: res.data.params.sender.phone,
              contactNumber: res.data.params.sender.classNo,
              showdetail: true,
              showdetail2: false,
              showdetail3: false,
            })
          } else if (res.data.params.sender.exam == "审核通过") {
            wx.setStorageSync('sender', res.data.params.sender);
            wx.switchTab({
              url: '/pages/run/run'
            })
          }else{
            wx.redirectTo({
              url: '/pages/bindUser/bindUser',
            })
          }
          wx.hideLoading()
        } else {
          wx.hideLoading()
          wx.redirectTo({
            url: '/pages/bindUser/bindUser',
          })
        }
      })
    }else{
      wx.redirectTo({
        url: '/pages/bindUser/bindUser',
      })
    }
  },

  //联系人
  contactPersonInput: function (e) {
    that.setData({
      contactPerson: e.detail.value
    })
  },

  //联系方式
  contactPhoneInput: function (e) {
    that.setData({
      contactPhone: e.detail.value
    })
  },

  //学号
  contactNumberInput: function (e) {
    that.setData({
      contactNumber: e.detail.value
    })
  },

  //提交函数
  submit: function (e) {
    if (that.data.schoolName == "请选择学校" || that.data.contactPerson == "" || that.data.contactPhone == "" || that.data.contactNumber == "") {
      wx.showToast({
        title: "未填完整信息",
        image: '/images/tanHao.png',
        duration: 2000,
        mask: true
      })
    } else if (that.data.sender) {
      wx.showLoading({
        title: '加载中',
        mask: true
      })
      app.post2('/ops/sender/update', {
        id: that.data.sender.id,
        openId: wx.getStorageSync("user").openId,
        schoolId: that.data.schoolId,
        exam: '待审核',
      }, function (res) {
        wx.hideLoading()
        if (res.data.code) {
          that.setData({
            showdetail: false,
            showdetail2: true,
          })
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000,
            mask: true,
          })
        }
      })
    } else {
      wx.showLoading({
        title: '加载中',
        mask: true
      })
      app.post2('/ops/sender/add', {
        name: that.data.contactPerson,
        phone: that.data.contactPhone,
        classNo: that.data.contactNumber,
        schoolId: that.data.schoolId,
      }, function (res) {
        wx.hideLoading()
        if (res.data.code) {
          that.setData({
            showdetail: false,
            showdetail2: true,
          })
        } else {
          wx.showToast({
            title: '您已经提交过一次申请，耐心等待审核',
            icon: 'none',
            duration: 2000,
            mask: true,
          })
        }
      })
    }
  },
  
  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '椰子校园配送版，一起跟我来赚外快吧！',
      path: '/pages/index/index',
    }
  },
})