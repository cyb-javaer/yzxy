package ops.school.service.impl;

import ops.school.api.dao.OrdersMapper;
import ops.school.api.dao.SchoolMapper;
import ops.school.api.dao.WxUserBellMapper;
import ops.school.api.dto.ShopTj;
import ops.school.api.dto.project.ProductOrderDTO;
import ops.school.api.entity.Orders;
import ops.school.api.service.*;
import ops.school.api.util.RedisUtil;
import ops.school.api.util.ResponseObject;
import ops.school.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Service
public class TOrdersServiceImpl implements TOrdersService {

    private final static Logger logger = LoggerFactory.getLogger(TOrdersServiceImpl.class);

    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private WxUserService wxUserService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private ShopService shopService;
    @Autowired
    private TShopCouponService tShopCouponService;
    @Autowired
    private ProductService productService;
    @Autowired
    private FloorService floorService;
    @Autowired
    private ProductAttributeService productAttributeService;
    @Autowired
    private OrderProductService orderProductService;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private TRunOrdersService tRunOrdersService;
    @Autowired
    private RunOrdersService runOrdersService;
    @Autowired
    private FullCutService fullCutService;
    @Autowired
    private WxUserBellService wxUserBellService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisUtil cache;
    @Autowired
    private TShopFullCutService tShopFullCutService;
    @Autowired
    private CouponService couponService;
    @Autowired
    private WxUserCouponService wxUserCouponService;

    @Autowired
    private TWxUserCouponService tWxUserCouponService;

    @Autowired
    private TCouponService tCouponService;
    @Autowired
    private OrderCompleteService orderCompleteService;
    @Autowired
    private SenderService senderService;
    @Autowired
    private ShopFullCutService shopFullCutService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private WxUserBellMapper wxUserBellMapper;

    @Autowired
    private SchoolMapper schoolMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Transactional
    @Override
    public void addTakeout(Integer[] productIds, Integer[] attributeIndex, Integer[] counts, @Valid Orders orders) {

    }
    /**
     * @date:   2019/7/19 18:16
     * @author: QinDaoFang
     * @version:version
     * @return: ops.school.api.util.ResponseObject
     * @param   productOrderDTOS --> 商品ID productId 商品规格ID attributeId 商品数量 count
     * @param   orders 包含微信用户openid，学校id，店铺id，楼栋id，订单类型type,
     *          配送费sendPrice, 餐盒费boxPrice, 用户优惠券ID wxUserCouponId, 实付款金额payPrice
     * @Desc:   desc 用户提交订单
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResponseObject addOrder2(List<ProductOrderDTO> productOrderDTOS, @Valid Orders orders) {
        return null;
    }

    @Transactional
    @Override
    public int pay(Orders orders,String formid) {
        return 0;
    }

    @Transactional
    @Override
    public int paySuccess(String orderId, String payment) {

        return 0;
    }

    @Transactional
    @Override
    public int cancel(String id) {

        return 0;
    }

    @Transactional
    @Override
    public int shopAcceptOrderById(String orderId) {

            return 0;
    }


    @Override
    public ShopTj shopstatistics(Integer shopId, String beginTime, String endTime) {

        return null;
    }

    @Override
    public Map countKindsOrderByBIdAndTime(Integer buildId, String beginTime, String endTime) {

        return null;
    }


    @Override
    public int orderSettlement(String orderId) {

        return 1;
    }

    /**
     * @date:   2019/8/6 15:40
     * @author: QinDaoFang
     * @version:version
     * @return: int
     * @param   orderComplete1
     * @Desc:   desc 二次修改 传orders结算少一个查库
     */
    @Transactional
    @Override
    public Boolean orderSettlementByOrders(Orders orders) {

        return true;
    }

}
