var app = getApp();
var that;

Page({

  data: {
    tabs: [
      { title: '跑腿订单', content: '内容二', active: false },
    ],
    tab: 0,
    orders: [],
    totalorders: 1,
    load: true,
  },

  showLoading() {
    this.setData({
      load: true
    })
  },
  hideLoading() {
    this.setData({
      load: false
    })
  },

  onLoad: function (options) {
    that = this
    that.setData({
      vertion: wx.getStorageSync("vertion")
    })
    that.showLoading()
  },

  // 切换头部
  onClick: function (e) {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    for (var i = 0; i < that.data.tabs.length; i++) {
      that.data.tabs[i].active = false;
    }
    var query = that.data.query;
    query.page = 1;
    if (e.currentTarget.dataset.index == 1) {
      that.findOrder2(that.data.query, false)
    } else {
      that.findOrder(that.data.query, false)
    }
    that.data.tabs[e.currentTarget.dataset.index].active = true;
    that.setData({
      tabs: that.data.tabs,
      tab: e.currentTarget.dataset.index
    })
  },

  onShow: function () {
    var query = {
      page: 1,
      size: 6,
      schoolId: wx.getStorageSync("schoolId"),
      openId: wx.getStorageSync("user").openId
    }
    that.setData({
      query: query
    })

      that.findOrder2(query, false)
  },

  //查找订单
  findOrder2: function (query, bottom) {
    app.post('/ops/runorders/find',
      query, function (res) {
        if (res.data.code) {
          //成功
          for (var i in res.data.params.list) {
            res.data.params.list[i].payPrice = (res.data.params.list[i].totalPrice).toFixed(2);
            switch (res.data.params.list[i].status) {
              case "待付款":
                res.data.params.list[i].style = 2;
                break;
              case "待接手":
                res.data.params.list[i].style = 1;
                break;
              case "商家已接手":
                res.data.params.list[i].style = 0;
                break;
              case "配送员已接手":
                res.data.params.list[i].style = 0;
                break;
              case "已完成":
                res.data.params.list[i].style = 3;
                break;
              case "已取消":
                res.data.params.list[i].style = 4;
                break;
            }
          }
          if (bottom == false) {
            that.setData({
              orders: res.data.params.list,
              totalorders: res.data.params.total,
            })
          } else {
            that.setData({
              orders: that.data.orders.concat(res.data.params.list)
            })
          }
          that.hideLoading()
          wx.hideLoading()
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000,
            mask: true,
          })
        }
      })
  },

  //前往详情
  navtos: function (e) {
    wx.navigateTo({
      url: "/pages/order/orderDetail/orderDetail?orderId=" + e.currentTarget.id + '&typ=' + that.data.orders[e.currentTarget.dataset.index].typ
    })
  },

  //联系骑手
  runPhone: function (e) {
    wx.makePhoneCall({
      phoneNumber: that.data.orders[e.currentTarget.dataset.index].senderPhone,
    })
  },

  //取消订单
  navtos2: function (e) {
    var api = '/ops/runorders/cancel'
    wx.showModal({
      title: '提示',
      content: '是否确认取消订单',
      showCancel: true,
      confirmColor: '#3797ee',
      confirmText: '确认',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '加载中',
            mask: true
          })
          app.post(api, {
            id: e.currentTarget.id
          }, function (res) {
            wx.hideLoading()
            if (res.data.code) {
              //成功
              wx.showToast({
                title: '取消成功',
                image: '/images/success.png',
                duration: 2000,
                mask: true
              })
              wx.navigateTo({
                url: "/pages/order/orderDetail/orderDetail?orderId=" + e.currentTarget.id + '&typ=' + e.currentTarget.dataset.typ
              })
            } else {
              wx.showToast({
                title: res.data.msg,
                icon: 'none',
                duration: 2000,
                mask: true,
              })
            }
          })
        }
      }
    })
  },

  //评论订单
  navtos3: function (e) {
    wx.navigateTo({
      url: "/pages/order/comment/comment?orderId=" + e.currentTarget.id + '&typ=' + that.data.orders[e.currentTarget.dataset.index].typ
    })
  },

  onReachBottom: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    var totalorders = that.data.totalorders;
    var query = that.data.query;
    query.page += 1;
    if (that.data.orders.length < totalorders && that.data.tab == 0) {
      that.findOrder(query, true)
    } else if (that.data.orders.length < totalorders && that.data.tab == 1){
      that.findOrder2(query, true)
    }else{
      wx.hideLoading()
    }
  },

  onPullDownRefresh: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    var query = that.data.query;
    query.page = 1;
    if (that.data.tab == 0){
      that.findOrder(that.data.query, false)
      wx.stopPullDownRefresh();
    } else if (that.data.tab == 1){
      that.findOrder2(that.data.query, false)
      wx.stopPullDownRefresh();
    }else{
    wx.hideLoading()
    }
  },

  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '快来和我一起享校园品质生活吧！！',
      path: '/pages/index/index',
    }
  },
})