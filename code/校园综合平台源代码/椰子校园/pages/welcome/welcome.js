var app = getApp();
var that;

Page({

  data: {
    showdetail: false,
    hoslist: [],
    chongxuan: 0,
    menu: 0,
  },

  onLoad: function (options) {
    that = this
    var chongxuan = options.chongxuan
    var menu = options.menu
    if (chongxuan == 1) {
      that.setData({
        chongxuan: options.chongxuan
      })
    } else if (menu != 0) {
      that.setData({
        menu: options.menu
      })
    }
  },

  onShow: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: app.globalData.IP + '/ops/school/find', //仅为示例，并非真实的接口地址
      data: {
        page: 1,
        size: 100,
        orderBy: 'sort desc',
        queryType: 'wxuser',
        wxAppId: 'wx0c193b30f0f8ac93'
      },
      dataType: 'json',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      success(res) {
        if (res.data.code) {
          //成功
          wx.hideLoading()
          var show = true;
          var schools = res.data.params.list;
          for (let i = 0; i < res.data.params.list.length; i++) {
            res.data.params.list[i].show = show;
          }
          wx.setStorage({
            key: 'hoslist',
            data: schools,
          })
          that.setData({
            hoslist: schools
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000,
            mask: true,
          })
        }
      }
    })
  },

  input: function (e) {
    this.search(e.detail.value)
  },

  search: function (key) {
    var hoslist = wx.getStorage({
      key: 'hoslist',
      success: function (res) {
        if (key == '') {
          that.setData({
            hoslist: res.data
          })
          return;
        }
        var arr = [];
        for (let i in res.data) {
          res.data[i].show = false;
          if (res.data[i].name.indexOf(key) >= 0) {
            res.data[i].show = true;
            arr.push(res.data[i])
          }
        }
        if (arr.length == 0) {
          that.setData({
            hoslist: [{
              show: true,
              name: "对不起，您搜索的学校暂未开通"
            }]
          })
        } else {
          that.setData({
            hoslist: arr
          })
        }
      },
    })
  },

  changeSchool: function () {
    that.setData({
      showdetail: true
    })
  },


  //点击保存
  returnApp: function (e) {
    let schoolId = that.data.hoslist[e.currentTarget.dataset.index].id;
    let schoolName = that.data.hoslist[e.currentTarget.dataset.index].name
      if (!wx.getStorageSync("user")) {
        //获取用户信息
        wx.showLoading({
          title: '加载中',
          mask: true
        })
        wx.login({
          success: function (res) {
            wx.request({
              url: app.globalData.IP + '/ops/user/wx/login', //仅为示例，并非真实的接口地址
              data: {
                code: res.code,
                schoolId: schoolId
              },
              dataType: 'json',
              method: 'GET',
              header: {
                'content-type': 'application/x-www-form-urlencoded',
              },
              success(res) {
                if (res.data.code) {
                  wx.setStorageSync('token', res.data.params.token);
                  if (e.detail.userInfo) {
                    let gender = '未知';
                    if (e.detail.userInfo.gender == 1) {
                      gender = '男';
                    } else if (e.detail.userInfo.gender == 2) {
                      gender = '女';
                    }
                    // if (e.detail.userInfo.nickName.match(/\ud83c[\udf00-\udfff]|\ud83d[\udc00-\ude4f]|\ud83d[\ude80-\udeff]/g) != null) {
                    //   e.detail.userInfo.nickName = encodeURI(e.detail.userInfo.nickName)
                    // }
                    app.post('/ops/user/wx/update', {
                      id: res.data.params.user.id,
                      schoolId: schoolId,
                      // openId: res.data.params.user.opend,
                      nickName: e.detail.userInfo.nickName,
                      avatarUrl: e.detail.userInfo.avatarUrl,
                      gender: gender,
                      province: e.detail.userInfo.province,
                      city: e.detail.userInfo.city
                    }, function (res) {
                      //用户信息更新成功
                      if (res.data.code) {
                        wx.hideLoading()
                        // wx.removeStorage({ key: 'hoslist', })
                        // res.data.params.user.nickName = decodeURI(res.data.params.user.nickName)
                        wx.setStorageSync('user', res.data.params.user);
                        wx.setStorageSync('schoolId', schoolId);
                        wx.setStorageSync('schoolName', schoolName);
                        if (that.data.chongxuan == 0 && that.data.menu == 0) {
                          wx.navigateBack({
                            delta: 1
                          })
                        } else if (that.data.menu != 0) {
                          wx.navigateTo({
                            url: '/pages/index/item/menu/menu?shopid=' + that.data.menu
                          })
                        } else {
                          wx.setStorageSync(
                            'chongxuang2',
                            1,
                          )
                          wx.switchTab({
                            url: '/pages/index/index'
                          })
                        }
                      } else {
                        wx.showToast({
                          title: res.data.msg,
                          icon: 'none',
                          duration: 2000,
                          mask: true,
                        })
                      }
                    })
                  } else {
                    wx.showToast({
                      title: "请允许授权登陆",
                      image: '/images/tanHao.png',
                      duration: 2000,
                      mask: true
                    })
                  }
                } else {
                  wx.hideLoading()
                }
              }
            })
          }
        })
      }else {
        wx.showLoading({
          title: '加载中',
          mask: true
        })
        wx.login({
          success: function (res) {
            wx.request({
              url: app.globalData.IP + '/ops/user/wx/login', //仅为示例，并非真实的接口地址
              data: {
                code: res.code,
                schoolId: schoolId
              },
              dataType: 'json',
              method: 'GET',
              header: {
                'content-type': 'application/x-www-form-urlencoded',
              },
              success(res) {
                if (res.data.code) {
                  //成功获取用户信息
                  wx.setStorageSync('token', res.data.params.token);
                  app.post('/ops/user/wx/update', {
                    schoolId: schoolId,
                    id: res.data.params.user.id,
                    // openId: res.data.params.user.openId,
                  }, function (res) {
                    //用户信息更新成功
                    if (res.data.code) {
                      wx.hideLoading()
                      // wx.removeStorage({ key: 'hoslist', })
                      // res.data.params.user.nickName = decodeURI(res.data.params.user.nickName)
                      wx.setStorageSync('schoolId', schoolId);
                      wx.setStorageSync('schoolName', schoolName);
                      wx.setStorageSync('user', res.data.params.user);
                      if (that.data.chongxuan == 0 && that.data.menu == 0) {
                        wx.navigateBack({
                          delta: 1
                        })
                      } else if (that.data.menu != 0) {
                        wx.navigateTo({
                          url: '/pages/index/item/menu/menu?shopid=' + that.data.menu
                        })
                      } else {
                        wx.setStorageSync(
                          'chongxuang2',
                          1,
                        )
                        wx.switchTab({
                          url: '/pages/index/index'
                        })
                      }
                    } else {
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000,
                        mask: true,
                      })
                    }
                  })
                } else {
                  wx.hideLoading()
                }
              }
            })
          }
        })
      }
  },

  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '快来和我一起享校园品质生活吧！！',
      path: '/pages/index/index',
    }
  },
})